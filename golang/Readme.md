Automotive Data Solution - Backend skill test - Golang
==========================

## Tasks

The list of the tasks are listed in the [Readme](../Readme.md) in the root directory of the bitbucket project.

## Requirement

* You need to have go v1.15.x at least to be able to use the go modules [Download Golang](https://golang.org/dl/)

* You need to install docker desktop and Sign Up [Download Docker](https://www.docker.com/products/docker-desktop)

## Structure of the project

```
golang
├── Readme.md                           # Readme
├── api                                 # Contain all the source file necessary for the api
│    ├── database.go                    # Contain the query to get the data from mysql
│    ├── models.go                      # Contain the model
│    ├── router.go                      # Contain all the endpoint of the api
│    └── routes.go                      # Contain the implementation of the handler related to the different endpoint of the api
│
├── data                                # The data
│    ├── postman                        # Postman file (collection/environment)
│    │      └── collection.json         # The postman collection of the project
│    └── sql                            # Folder of sql files use by MySQL
│         ├── create_database.sql       # SQL file to create the database
│         ├── load_fixtures.sql         # SQL file to load the fixtures
│         └── update_database.sql       # SQL file to ypdate the database
│
├── docker-compose.yml                  # Docker Compose for the database MySQL
├── go.mod                              # List of go modules used by the project
├── main.go                             # Main file to run the project
└── runmysql.sh                         # Script to run/stop MySQL
```

Notes: 

* You are free to organize the content of the folder `api` as you want. 
* You are free to install other go module 
* You have to use the **gin** framework pre-installed

## How to run the project

### Start/Stop MySQL

1) Start the database MySQL
```
./runmysql.sh  #Choose start in the contextual menu
```

2) Stop the database MySQL
```
./runmysql.sh  #Choose stop in the contextual menu
```

3) Stop and Purge the database MySQL
```
./runmysql.sh  #Choose purge in the contextual menu
```

**IMPORTANT**: Everytime you modify the file `data\sql\update_database.sql` you have to run the script `runmysql.sh` with the command *purge* 

Note: You can apply directly your SQL queries to test them in your favorite Mysql UI by using the credential defined in the `.env.default`

### Run API

1) Create a file `.env` based on `.env.default` at the same level

2) Start the go project

``` 
go build && ./backendapi
```

4) Check that everything is ok

``` 
curl http://localhost:8080/api/vehicle-makes
```

Sample response

```
{
    "vehicle_makes": [
        {
            "id": 1,
            "name": "Acura",
            "url": "https://www.acura.ca"
        },
        {
            "id": 2,
            "name": "Alfa Romeo",
            "url": "https://www.alfaromeo.ca"
        }
    ]
}
```

### Documentation
This app use [Gin](https://github.com/gin-gonic/gin). This is a web framework written in Go (Golang).
Documentation can be found here: https://github.com/gin-gonic/gin

We use docker compose to have a database MySQL in local accessible by this project.
The documentation of how to use *docker-compose* can be found here: https://docs.docker.com/compose/